const express = require("express");
const router = express.Router();
const User = require("../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const { check, validationResult } = require("express-validator");

// @route   POST api/users
// @desc    Register a user
// @access  Public
router.post(
  "/",
  [
    check("name", "Please insert a name")
      .not()
      .isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).isLength({ min: 6 })
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password } = req.body;

    try {
      // get User by Email --> email: email = email
      let user = await User.findOne({ email });

      if (user) {
        return res.status(400).json({ msg: "User already exists" });
      }

      user = new User({ name, email, password });

      // create random Salt
      const salt = await bcrypt.genSalt(10);
      // encrypt password with salt / hash stuff
      user.password = await bcrypt.hash(password, salt);

      await user.save();

      // send the user
      const payload = {
        user: {
          id: user.id
        }
      };

      // get a unique token for a user to work with
      jwt.sign(
        payload,
        config.get("jwtSecret"),
        {
          expiresIn: 3600000 // 3600 / 1 hour hour by default, 3600000 for no expire while testing
        },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server unavailable");
    }
  }
);

module.exports = router;
