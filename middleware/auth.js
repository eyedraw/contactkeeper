const jwt = require('jsonwebtoken');
config = require('config');

// middleware --> create next function "go to the next piece of middleware"
module.exports = function(req, res, next) {
    // Get token from header by x-auth-token key name
    const token = req.header('x-auth-token');

    // Check if not token
    if(!token) {
        return res.status(401).json({msg: 'No authorization. No token'});
    }

    try {
        const decoded = jwt.verify(token, config.get('jwtSecret'));

        // returns user id to request, so we have access to it inside the route
        req.user = decoded.user;
        next();
    } catch (err) {
        res.status(401).json({ msg: 'token is not valid' });
    }
}