const mongoose = require("mongoose");

const ContactSchema = mongoose.Schema({
  // each user need his own set of contact
  // Your reference is the 'users' table in your database.
  // Even though your mongoose model identifies as a single user,
  // when MongoDB auto creates that table for you using the mongoose model, it will pluralize whatever you wrote
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  phone: {
    type: String
  },
  type: {
    type: String,
    default: "personal"
  },
  date: {
    type: Date,
    default: Date.now
  }
});

let contacts;
try {
  contacts = mongoose.model("contact");
} catch (error) {
  contacts = mongoose.model("contact", ContactSchema);
}

module.exports = contacts;
