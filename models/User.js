const mongoose = require("mongoose");
const UserSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

let users;
try {
  users = mongoose.model("user");
} catch (error) {
  users = mongoose.model("user", UserSchema);
}

module.exports = users;
